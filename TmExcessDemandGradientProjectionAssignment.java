package com.kolovsky.java;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import java.util.*;
import java.util.stream.*;
import java.util.HashSet; // Import the HashSet class

public class TmExcessDemandGradientProjectionAssignment {

    /**
     * Default maximal number of Newton method iterations for shifting of flows between minimal and maximal path
     */
    public static final int DEFAULT_NEWTON_METHOD_ITERATIONS_COUNT = 100;
    /**
     * Connection to the database with the graph and the origin-destination matrix
     */
    protected TmDbConnector dbConnector;
    /**
     * Maximal number of iterations of the GPA Excess demand traffic assignment
     */
    protected int iterationsCount;
    /**
     * Maximal desired epsilon of the GPA Excess demand traffic assignment
     */
    protected double epsilon;
    /**
     * Currently performed iteration of the GPA Excess demand traffic assignment
     */
    protected int currentIteration;
    /**
     * Cost function used for calculation of the costs of individual edges and also for integral and derivation of the costs
     */
    protected TmBCostFunction costFunction;
    /**
     * Excess Demand function for calculation of Modal split
     */
    protected TmExcessDemandCost excessDemandFunction;
    /**
     * The graph representing the road traffic network
     */
    protected TmGraph graph;


    /**
     * The origin-destination matrix representing the flows of vehicles from origin nodes to destination nodes for the road traffic network
     */
    protected TmOriginDestinationMatrix originDestinationMatrix;

    protected TmOriginDestinationPair originDestinationPair;

    /**Parallel all-or-nothing static traffic assignment*/
    protected TmParallelAllOrNothingStaticTrafficAssignment parallelAllOrNothingStaticTrafficAssignment;

    /**
     * PARALLEL COMPUTATITON STUFF.
     */
    protected TmParallelBWorkingThread[] workingThreads;

    /**
     * PARALLEL COMPUTATITON STUFF 2. Not sure if it will be used
     */
    protected TmCallbackInvokable callbackInvokable;
    /**
     * Determines whether synchronous or asynchronous computation shall be used
     */
    protected boolean synchronousComputation;
    /** TERMINATION */
    /**
     * Determines whether the computation is terminated
     */
    protected boolean terminated;

    /**
     * Origins zones array in order to calculate shortest paths tree from all origins
     */
    protected TmZone [] origins;

    protected TmZone origin;

    /** Number of edges of the graph */
    protected int number_of_edges;


    /** DECLARATION OF CONSTANTS*/
    /**
     * Parameter theta of logit modal split function
     */
    protected double theta;
    /**
     * Average walking speed of the passenger
     */
    protected double walkingSpeed;
    /** Average waiting time of passenger at the bus stop*/
    protected double average_waiting_time;
    /** Average dwell time of bus at the bus stop, when passenger is not egressing and continuing along the line*/
    protected double average_dwell_time;
    /** Average boarding time of passenger at the bus stop*/
    protected double average_boarding_time;
    /**
     * Initial costs of the edges of the graph
     */
    protected double[] initialEdgesCosts;

    /**
     * Final flows assigned to the edges of the graph once the computation is finished
     */
    protected double[] assignedEdgesFlows;
    /**
     * Auxiliary Array of costs used to store updated costs after new assignment
     */
    protected double [] edgesCosts_copy;
    /**
     * Auxiliary Array of edges used for calculation of the shortest path tree from each origin in the whole network
     */
    protected TmEdge[] nodesPreviousEdges ;

    /**
     * Total travel demand of the OD pair
     */
    protected double demandTotal;
    /**
     * Auto travel demand of the OD pair
     */
    protected double demandAuto;
    /**
     * Transit travel demand of the OD pair
     */
    protected double demandModeB;
    /**
     * Resulting Demand array for transit OD pair demand
     */
    protected double [] demandModeBfinal;
    /**
     * Array of edges for storing the shortest path
     */
    protected TmEdge[] shortest_path;

    List<TmEdge[]> UsedPathSetList = new ArrayList<>();

    List<TmEdge[]> PathsToRemove = new ArrayList<>();

    /**
     * Array for storing Bus path's edges travel costs
     */
    protected double [] bus_paths_edges_travel_costs;
    /**
     * Travel costs of whole bus path ( sum of edges costs) from origin r to destination s
     */
    protected double bus_path_travel_cost;
    /**
     * Array for storing Auto shortest path's edges travel costs
     */
    protected double [] auto_paths_edges_travel_costs;
    /**
     * Array for storing shortest path's edges travel costs
     */
    protected double [] shortest_paths_edges_travel_costs;
    /**
     * Travel costs of whole auto path ( sum of edges costs) from origin r to destination s
     */
    protected double auto_path_travel_cost;
    /**
     * Travel costs of shortest auto path ( sum of edges costs) from origin r to destination s
     */
    protected double shortest_path_travel_cost;
    /**
    * Excess demand cost of OD pair rs
    */
    protected double excessDemandCost;
    /** Maximal paths costs calculation:*/
    /**
     * Auxiliary array for storing paths costs from used path list
     */
    protected double [] pathsCosts;

    /**
     * Auxiliary array for storing edges costs from used path list
     */
    protected double [] edgesCosts;
    /**
     * Auxiliary variable for storing maximal costs from used path list in order to find the path with maximal costs
     */
    protected double maxPathCost;
    /**
    * Path with maximal travel costs which will be used to shift travelers from to the shortest path
    */
    TmEdge [] max_path;

    /**
     * Array for storing maximal path's edges travel costs
     */
    protected double [] max_paths_edges_travel_costs;
    /**
     * Travel costs of whole maximal path ( sum of edges costs) from origin r to destination s
     */
    protected double max_path_travel_cost;

    protected double [] shortest_paths_edges_travel_costs_derivatives;
    /**
     * Auxiliary array for storing maximal paths edges ids, used for calculation of costs functions derivatives
     */
    protected int [] max_paths_edges_id;
    /**
     * Auxiliary array for storing shortest paths edges ids, used for calculation of costs functions derivatives
     */
    protected int []shortest_paths_edges_id;

    /**
     * List for storing cost function derivatives of the edges that are either on the shortest path or maximal path but not both
     */
    List <Double> unique_edges_derivatives_list = new ArrayList<>();
    /**
     * Sum of the derivatives of the edges that are either on the shortest path or maximal path but not both
     */
    protected double unique_edges_derivatives_sum;
    /**
    Array for storing the derivatives of the shortest path's edges
     */
    protected double [] shortest_path_edges_derivatives;
    /**
     * Sum of the derivatives of the shortest path's edges
     */
    protected double shortest_path_edges_derivatives_sum;
    /**
     Array for storing the derivatives of the maximal path's edges
     */
    protected double [] max_path_edges_derivatives;
    /**
     Sum of the derivatives of the maximal path's edges
     */
    protected double max_path_edges_derivatives_sum;
    /**
     Array for storing the derivatives of the excess demand costs
     */
    protected double [] excess_demand_cost_derivatives;
    /**
     Sum of the excess demand cost derivatives
     */
    protected double excess_demand_cost_derivatives_sum;

    /**
     * Maximal path flow for storing updated flow
     */
    protected double maximal_path_flow;
    /**
     * Shortest path flow for storing updated flow
     */
    protected double shortest_path_flow;

    protected double previous_shortest_path_flow;

    protected double previous_maximal_path_flow;

    protected double previous_demand_modeB;

    protected double other_paths_flow_sum;

    protected double [] bus_paths_edges_travel_costs_derivatives;

    protected double [] edgesCosts_bus;

    protected double bus_paths_edges_travel_costs_derivatives_sum;
    /**
     * Excess demand cost from previous iteration, used to decide which path the buses will use
     */
    protected double excess_demand_cost_previous;

    /**
     * Creates new instance for the GPA Excess Demand Modal split traffic assignment
     * @param originDestinationMatrix the origin-destination matrix with the set graph and model name - both necessary for the GPA assignment
     * @param iterationsCount         maximal number of iterations of the GPA static traffic assignment
     * @param epsilon                 maximal desired epsilon of the GPA static traffic assignment
     * @param workingThreadsCount     the number of working threads, which shall be created to perform the parallel computation
     * @param callbackInvokable       reference to an instance, which is used to perform callback with intermediate results during each iteration
     * @param theta                   parameter of the logit modal split function
     * @param walkingSpeed            walking speed of the passenger
     * @param average_waiting_time    average waiting time of the passenger
     * @param average_boarding_time   average boarding time of the passenger
     * @param average_dwell_time      average dwell time of vehicle on while being at the stop
     */

    public TmExcessDemandGradientProjectionAssignment (TmOriginDestinationMatrix originDestinationMatrix, int iterationsCount, double epsilon,
                                          int workingThreadsCount, TmCallbackInvokable callbackInvokable, double theta, double walkingSpeed, double average_waiting_time,
                                          double average_boarding_time, double average_dwell_time) {
        this.originDestinationMatrix = originDestinationMatrix;
        this.iterationsCount = iterationsCount;
        this.epsilon = epsilon;
        this.callbackInvokable = callbackInvokable;
        this.theta = theta;
        this.walkingSpeed = walkingSpeed;
        this.average_waiting_time = average_waiting_time;
        this.average_boarding_time = average_boarding_time;
        this.average_dwell_time = average_dwell_time;
        // Initialization
        synchronousComputation = false;
        terminated = false;
        graph = originDestinationMatrix.getGraph();
        costFunction = new TmBCostFunction();
        excessDemandFunction = new TmExcessDemandCost();
        origins = originDestinationMatrix.getOrigins();

        number_of_edges = graph.getEdges().size();

        //Obtain intial costs of edges in the graph
        initialEdgesCosts = new double[number_of_edges]; // declare array


        for (int i = 0; i < number_of_edges; i++) {// for all edges of the graph
            initialEdgesCosts[i] = graph.getEdges().get(i).getCost();// store the values into the array
        }
        /* Initialize AON car assignment*/
        parallelAllOrNothingStaticTrafficAssignment = new TmParallelAllOrNothingStaticTrafficAssignment(graph, originDestinationMatrix, initialEdgesCosts, workingThreadsCount);

        //Declaring an array for storing final flows
        assignedEdgesFlows = new double[number_of_edges];
        //Declaring an array in which the updated costs will be stored throughout the calculation
        edgesCosts_copy = Arrays.copyOf(initialEdgesCosts, initialEdgesCosts.length);
        edgesCosts_bus = Arrays.copyOf(initialEdgesCosts, initialEdgesCosts.length);


    }
    public void CalculateGPA() {
        currentIteration = 0;
        while(currentIteration >= 0) {


                // Perform initial assignment
                for (int n =0; n < origins.length; n++) {

                    origin = origins[n];
                    //Determine shortest cost tree from origin with initial costs
                    nodesPreviousEdges = graph.determineNodesPreviousEdges(origin, initialEdgesCosts);

                    for (int i = 0; i < originDestinationMatrix.getOriginDestinationsCount(origin.getId()); i++) {
                        //Store origin destination pair
                        originDestinationPair = originDestinationMatrix.getOriginDestinationPair(origin.getId(), i);

                        if (currentIteration == 0) {

                        //Find the shortest path of the od pair and store it
                        shortest_path = graph.extractPath(originDestinationPair.getDestination(), nodesPreviousEdges);
                        //Add the path to the path set
                        UsedPathSetList.add(shortest_path);

                        // INITIALIZATION: x_a = 0 q_rs^B = q_rs
                        demandTotal = originDestinationPair.getFlow();
                        demandAuto = demandTotal/ 2;
                        demandModeB = demandTotal/ 2;
                        //Declaring an array for storing the travel costs on bus path
                        bus_paths_edges_travel_costs= new double[shortest_path.length];
                        //Declaring an array for storing the travel costs on auto path
                        shortest_paths_edges_travel_costs= new double[shortest_path.length];

                        //COLUMN GENERATION:
                        //Perform an initial AoN assignment on the shortest path
                        for (int k = 0; k < shortest_path.length; k++) {
                            shortest_path[k].setFlow(demandAuto);
                            shortest_paths_edges_travel_costs[k]= costFunction.calculateCost(shortest_path[k].getFlow(),shortest_path[k].getCapacity(),shortest_path[k].getCost());

                        }
                        //Update excess demand costs
                        //First, calculate costs of transit mode B
                        for (int j = 0; j < shortest_path.length; j++) {

                            bus_paths_edges_travel_costs[j] = // stop is right at source, no need to walk:
                                    shortest_path[j].getIs_source_connector()*shortest_path[j].getHasStop()* (average_waiting_time + average_boarding_time)
                                            //passenger is walking from source to stop:
                                            +shortest_path[j].getIs_source_connector()*(1-shortest_path[j].getHasStop()) * shortest_path[j].getLength()/walkingSpeed
                                            //if passenger has reached the stop and is going to board:
                                            +(1-shortest_path[j].getIs_source_connector())*shortest_path[j].getHasStop()* (average_waiting_time + average_boarding_time)
                                            // otherwise passenger continue walking:
                                            //Following line is a modification so restricted graph do not have to be used
                                            // when walking instead of traveling in vehicle on edges without transit line, stop, or being zonal connectors:
                                            +(1-shortest_path[j].getIs_source_connector())* (1-shortest_path[j].getHasStop())*(1-shortest_path[j].getIs_target_connector())
                                            *(1-shortest_path[j].getHasTransit())*shortest_path[j].getLength()/walkingSpeed
                                            //+(1-bus_shortest_path[j].getIs_source_connector())*(1- bus_shortest_path[j].getHasStop())* bus_shortest_path[j].getLength()/walkingSpeed
                                            // in vehicle travel time :
                                            +shortest_path[j].getHasTransit() * costFunction.calculateCost((0.95)*shortest_path[j].getFlow(),shortest_path[j].getCapacity(),shortest_path[j].getCost())
                                            // dwelling times on the stops along the line:
                                            +shortest_path[j].getHasStop()*(1-shortest_path[j].getIs_target_connector())*average_dwell_time
                                            //stop is right at target, no need to walk, just do unboard:
                                            +shortest_path[j].getHasStop()*shortest_path[j].getIs_target_connector()*average_boarding_time
                                            // if bus passes by target edge but passenger, which has already left the vehicle,
                                            // is walking on the target edge towards hers/his destination,
                                            // we need to subtract the time contribution of the riding bus on the same walking edge:
                                            -shortest_path[j].getIs_target_connector()* shortest_path[j].getHasTransit() * (1-shortest_path[j].getHasStop())
                                            * costFunction.calculateCost((0.95)*shortest_path[j].getFlow(),shortest_path[j].getCapacity(),shortest_path[j].getCost())
                                            // if bus passes by source edge but passenger is walking from source to the nearest stop,
                                            // we need to subtract the time contribution of the riding bus on the same walking edge:
                                            -shortest_path[j].getHasTransit() * (1-shortest_path[j].getHasStop()) * shortest_path[j].getIs_source_connector()
                                            * costFunction.calculateCost((0.95)*shortest_path[j].getFlow(),shortest_path[j].getCapacity(),shortest_path[j].getCost())
                                            // if passenger has left the bus and is walking towards destination
                                            +shortest_path[j].getIs_target_connector() * (1-shortest_path[j].getHasStop()) * shortest_path[j].getLength()/walkingSpeed;
                        }
                        //COMPUTE PATH COST AND EXCESS DEMAND COSTS:
                        //Sum array elements to get cost of whole auto path
                        shortest_path_travel_cost= Arrays.stream(shortest_paths_edges_travel_costs).parallel().sum();
                        //Sum array elements to get cost of whole bus path
                        bus_path_travel_cost= Arrays.stream(bus_paths_edges_travel_costs).parallel().sum();
                        //Update excess demand costs
                        excessDemandCost = excessDemandFunction.calculate_Excess_demand_Cost(theta,bus_path_travel_cost,demandTotal,demandModeB);

                        //sets this flow on corresponding edges on the graph
                        for (int j = 0; j < number_of_edges; j++) { // for all edges on graph
                            for (int k = 0; k < shortest_path.length; k++) {// for all edges on the maximal path
                                if (graph.getEdges().get(j).getId() == shortest_path[k].getId()) {//finds the corresponding edge on the graph
                                    graph.getEdges().get(j).setFlow(shortest_path[k].getFlow());
                                }
                            }
                        }

                        //sets this flow on corresponding edges on the used path set list
                        for (int j = 0; j < UsedPathSetList.size(); j++) {// for all paths in the path set
                            for (int k =0; k <UsedPathSetList.get(j).length; k++){// for all edges of the list j-th element
                                for (int l = 0; l < shortest_path.length; l++) {// for all edges on the maximal path
                                    if (UsedPathSetList.get(j)[k].getId() == shortest_path[l].getId()) {//finds the corresponding edge
                                        UsedPathSetList.get(j)[k].setFlow(shortest_path[k].getFlow());
                                    }}}}

                        // updates costs
                        for (int j = 0; j < number_of_edges; j++) {// for all edges of the graph
                            for (int k = 0; k < shortest_path.length; k++) {
                                edgesCosts_copy[j] = costFunction.calculateCost(graph.getEdges().get(j).getFlow(),graph.getEdges().get(j).getCapacity(),graph.getEdges().get(j).getCost());// store the values into the array
                            }
                        }

                    //}
                //}

                currentIteration=1;// increment iteration counter
                 }

            else {

                // Find maximal path with maximal travel cost from which will be used to shift flows from
                for (int j = 0; j < UsedPathSetList.size(); j++) {// for all paths from path set
                    pathsCosts = new double[UsedPathSetList.size()];
                    edgesCosts= new double[UsedPathSetList.get(j).length];
                    for (int k = 0; k < UsedPathSetList.get(j).length; k++) {//for all edges on the path
                        //calculated costs of the k-th edge on the j-th path
                        edgesCosts[k] =costFunction.calculateCost(UsedPathSetList.get(j)[k].getFlow(),UsedPathSetList.get(j)[k].getCapacity(),UsedPathSetList.get(j)[k].getCost());
                        }
                    pathsCosts[j] = Arrays.stream(edgesCosts).parallel().sum();
                     }
                maxPathCost= Arrays.stream(pathsCosts).parallel().max().getAsDouble();
                for (int j = 0; j < UsedPathSetList.size(); j++) {// for all paths from path set
                if(maxPathCost == pathsCosts[j] ){max_path = UsedPathSetList.get(j);}}

                //Proceed with shifting flows from maximal path to the shortest path
                //For each origin

                //for (TmZone origin : origins) {

                    // Find new shortest paths tree with updated costs
                    nodesPreviousEdges = graph.determineNodesPreviousEdges(origin, edgesCosts_copy);

                    //for (int i = 0; i < originDestinationMatrix.getOriginDestinationsCount(origin.getId()); i++) {

                        excess_demand_cost_previous = excessDemandCost;

                        shortest_path = graph.extractPath(originDestinationPair.getDestination(), nodesPreviousEdges);
                        // check if the new shortest path is already in the set, if not, add it to the list of used paths
                        if (!UsedPathSetList.contains(shortest_path)) {
                            UsedPathSetList.add(shortest_path);
                        }
                        //Calculate shortest path cost
                        shortest_paths_edges_travel_costs= new double[shortest_path.length];
                        for (int k = 0; k < shortest_path.length; k++) {
                            shortest_paths_edges_travel_costs[k]= costFunction.calculateCost(shortest_path[k].getFlow(),shortest_path[k].getCapacity(),shortest_path[k].getCost());
                        }
                        //Sum array elements to get cost of whole shortest path
                        shortest_path_travel_cost= Arrays.stream(shortest_paths_edges_travel_costs).parallel().sum();

                        //Calculate maximal path cost
                        max_paths_edges_travel_costs= new double[max_path.length];
                        for (int j = 0; j < max_path.length; j++) {
                            max_paths_edges_travel_costs[j]= costFunction.calculateCost(max_path[j].getFlow(),max_path[j].getCapacity(),max_path[j].getCost());
                        }
                        //Sum array elements to get cost of whole maximal travel cost path
                        max_path_travel_cost= Arrays.stream(max_paths_edges_travel_costs).parallel().sum();

                        //Update excess demand costs
                        //First, calculate costs of transit mode B

                            for (int j = 0; j < max_path.length; j++) {
                                //if (max_path[j].getFlow() ==0) {max_path[j].setFlow(1);}
                                bus_paths_edges_travel_costs[j] = // stop is right at source, no need to walk:
                                        shortest_path[j].getIs_source_connector()*max_path[j].getHasStop()* (average_waiting_time + average_boarding_time)
                                                //passenger is walking from source to stop:
                                                +max_path[j].getIs_source_connector()*(1-max_path[j].getHasStop()) * max_path[j].getLength()/walkingSpeed
                                                //if passenger has reached the stop and is going to board:
                                                +(1-max_path[j].getIs_source_connector())*max_path[j].getHasStop()* (average_waiting_time + average_boarding_time)
                                                // otherwise passenger continue walking:
                                                //Following line is a modification so restricted graph do not have to be used
                                                // when walking instead of traveling in vehicle on edges without transit line, stop, or being zonal connectors:
                                                +(1-max_path[j].getIs_source_connector())* (1-max_path[j].getHasStop())*(1-max_path[j].getIs_target_connector())
                                                *(1-max_path[j].getHasTransit())*max_path[j].getLength()/walkingSpeed
                                                //+(1-bus_shortest_path[j].getIs_source_connector())*(1- bus_shortest_path[j].getHasStop())* bus_shortest_path[j].getLength()/walkingSpeed
                                                // in vehicle travel time :
                                                +max_path[j].getHasTransit() * costFunction.calculateCost((0.95)*max_path[j].getFlow(),max_path[j].getCapacity(),max_path[j].getCost())
                                                // dwelling times on the stops along the line:
                                                +max_path[j].getHasStop()*(1-max_path[j].getIs_target_connector())*average_dwell_time
                                                //stop is right at target, no need to walk, just do unboard:
                                                +max_path[j].getHasStop()*max_path[j].getIs_target_connector()*average_boarding_time
                                                // if bus passes by target edge but passenger, which has already left the vehicle,
                                                // is walking on the target edge towards hers/his destination,

                                                // we need to subtract the time contribution of the riding bus on the same walking edge:
                                                -max_path[j].getIs_target_connector()* max_path[j].getHasTransit() * (1-max_path[j].getHasStop())
                                                * costFunction.calculateCost((0.95)*max_path[j].getFlow(),max_path[j].getCapacity(),max_path[j].getCost())
                                                // if bus passes by source edge but passenger is walking from source to the nearest stop,

                                                // we need to subtract the time contribution of the riding bus on the same walking edge:
                                                -max_path[j].getHasTransit() * (1-max_path[j].getHasStop()) * max_path[j].getIs_source_connector()
                                                * costFunction.calculateCost((0.95)*max_path[j].getFlow(),max_path[j].getCapacity(),max_path[j].getCost())

                                                // if passenger has left the bus and is walking towards destination
                                                +max_path[j].getIs_target_connector() * (1-max_path[j].getHasStop()) * max_path[j].getLength()/walkingSpeed;

                                bus_paths_edges_travel_costs_derivatives_sum += max_path[j].getHasTransit() *0.95* costFunction.calculateCost((0.95)*max_path[j].getFlow(),max_path[j].getCapacity(),max_path[j].getCost())-max_path[j].getIs_target_connector()* max_path[j].getHasTransit() * (1-max_path[j].getHasStop())
                                        * 0.95 * costFunction.calculateCost((0.95)*max_path[j].getFlow(),max_path[j].getCapacity(),max_path[j].getCost())
                                        -max_path[j].getHasTransit() * (1-max_path[j].getHasStop()) * max_path[j].getIs_source_connector()
                                        * 0.95 *costFunction.calculateCost((0.95)*max_path[j].getFlow(),max_path[j].getCapacity(),max_path[j].getCost());

                            //Sum array elements to get cost of whole bus path
                            bus_path_travel_cost= Arrays.stream(bus_paths_edges_travel_costs).parallel().sum();
                        }

                        //COMPUTE PATH COST AND EXCESS DEMAND COSTS:
                        //Update excess demand costs
                        excessDemandCost = excessDemandFunction.calculate_Excess_demand_Cost(theta,bus_path_travel_cost,demandTotal,demandModeB);
                        //Derivatives calculations:
                        //Auxiliary arrays for finding unique edges
                        max_paths_edges_id= new int[max_path.length];
                        shortest_paths_edges_id= new int[shortest_path.length];

                        for (int j = 0; j < max_path.length; j++) {
                            max_paths_edges_id[j] =max_path[j].getId();}
                        for (int k = 0; k < shortest_path.length; k++) {
                            shortest_paths_edges_id[k]= shortest_path[k].getId();
                        }

                        List<Integer> path_unique_edges_ids_list = new ArrayList<>();//create a list or Integers
                        //add the values of the two arrays in this list
                        path_unique_edges_ids_list.addAll(Arrays.stream(max_paths_edges_id).boxed().collect(Collectors.toList()));
                        path_unique_edges_ids_list.addAll(Arrays.stream(shortest_paths_edges_id).boxed().collect(Collectors.toList()));

                        //we need a set to check if the element is duplicate or not
                        Set<Integer> set = new HashSet();
                        List<Integer> result = new ArrayList<>(path_unique_edges_ids_list);

                        //loop throw your list, and check if you can add this element to the set
                        // or not, if not this mean it is duplicate you have to remove it from your list
                        path_unique_edges_ids_list.stream().filter((j) -> (!set.add(j))).forEachOrdered((j) -> {
                            result.removeAll(Collections.singleton(j));
                        });

                        for (int j = 0; j < max_path.length; j++) {
                            for (int l = 0; l < result.size(); l++) {
                                if (max_path[j].getId() ==result.get(l) ) {unique_edges_derivatives_list.add(costFunction.calculateDerivation(max_path[j].getFlow(), max_path[j].getCapacity(), max_path[j].getCost()));}
                            }}
                            for (int k = 0; k < shortest_path.length; k++) {
                                for (int l = 0; l < result.size(); l++) {
                                    if (shortest_path[k].getId() ==result.get(l)){unique_edges_derivatives_list.add(costFunction.calculateDerivation(shortest_path[k].getFlow(),shortest_path[k].getCapacity(),shortest_path[k].getCost()));}
                                }
                                }

                        // Sum the unique edges derivatives
                        // This sum is used in the flow update equation when increasing the auto demand
                        for (int j = 0; j < unique_edges_derivatives_list.size(); j++) {
                            unique_edges_derivatives_sum += unique_edges_derivatives_list.get(j);
                        }
                        //Shortest path only derivatives
                        // Used in the excess demand update equation when increasing the auto demand
                        shortest_path_edges_derivatives = new double [shortest_path.length];
                        for (int k = 0; k < shortest_path.length; k++) {
                            shortest_path_edges_derivatives[k] = costFunction.calculateDerivation(shortest_path[k].getFlow(),shortest_path[k].getCapacity(),shortest_path[k].getCost());
                        }
                        shortest_path_edges_derivatives_sum= Arrays.stream(shortest_path_edges_derivatives).parallel().sum();
                        //Maximal path only derivatives
                        // Used in the flow update equation when increasing the transit demand
                        max_path_edges_derivatives = new double [max_path.length];
                        for (int j = 0; j < max_path.length; j++) {
                            max_path_edges_derivatives[j]= costFunction.calculateDerivation(max_path[j].getFlow(), max_path[j].getCapacity(), max_path[j].getCost());
                            //Auxiliary:
                            previous_maximal_path_flow = max_path[max_path.length-1].getFlow();
                        }
                        max_path_edges_derivatives_sum= Arrays.stream(max_path_edges_derivatives).parallel().sum();

                        //Excess demand cost derivatives
                        excess_demand_cost_derivatives_sum = excessDemandFunction.calculateExcess_demand_cost_Derivative(theta,bus_path_travel_cost,demandTotal,demandModeB);
                        previous_demand_modeB = demandModeB;
                        //Auxiliary, used in shortest path flow update equation
                        previous_shortest_path_flow =shortest_path[0].getFlow();

                        //COMPARE THE TRAVEL TIME BETWEEN AUTO SHORTEST PATH AND EXCESS DEMAND COST:
                        if (shortest_path_travel_cost < excessDemandCost) {

                            maximal_path_flow = previous_maximal_path_flow - (Math.min(previous_maximal_path_flow, (1/(unique_edges_derivatives_sum))*(max_path_travel_cost-shortest_path_travel_cost) ));

                            //maximal_path_flow = (Math.max(0.01,previous_maximal_path_flow - (1/(unique_edges_derivatives_sum))*(max_path_travel_cost-shortest_path_travel_cost) ));
                            //sets this flow on corresponding edges on the graph
                            for (int j = 0; j < number_of_edges; j++) { // for all edges on graph
                                for (int k = 0; k < max_path.length; k++) {// for all edges on the maximal path
                                    max_path[k].setFlow(maximal_path_flow);
                                    if (graph.getEdges().get(j).getId() == max_path[k].getId()) {//finds the corresponding edge on the graph
                                        graph.getEdges().get(j).setFlow(maximal_path_flow);}
                                }
                            }
                            //sets this flow on corresponding edges on the used path set list
                            for (int j = 0; j < UsedPathSetList.size(); j++) {// for all paths in the path set
                                for (int k =0; k <UsedPathSetList.get(j).length; k++){// for all edges of the list j-th element
                                for (int l = 0; l < max_path.length; l++) {// for all edges on the maximal path
                                    if (UsedPathSetList.get(j)[k].getId() == max_path[l].getId()) {//finds the corresponding edge
                                        UsedPathSetList.get(j)[k].setFlow(maximal_path_flow);
                            }}}}

                            for (int j = 0; j < UsedPathSetList.size(); j++) {// for all paths from path set
                                other_paths_flow_sum += UsedPathSetList.get(j)[0].getFlow();
                            }
                            //decreasing transit demand:

                            //demandModeB = previous_demand_modeB - Math.min(previous_demand_modeB, (1/ (shortest_path_edges_derivatives_sum+excess_demand_cost_derivatives_sum+bus_paths_edges_travel_costs_derivatives_sum))*
                              //      (excessDemandCost-shortest_path_travel_cost));

                            demandModeB = previous_demand_modeB - Math.min(previous_demand_modeB, (1/ (shortest_path_edges_derivatives_sum+excess_demand_cost_derivatives_sum))*
                                    (excessDemandCost-shortest_path_travel_cost));

                            //demandModeB = (Math.max(0.01,previous_demand_modeB - (1/ (shortest_path_edges_derivatives_sum+excess_demand_cost_derivatives_sum+bus_paths_edges_travel_costs_derivatives_sum))*
                                    //(excessDemandCost-shortest_path_travel_cost)));
                            shortest_path_flow= demandTotal - (other_paths_flow_sum-previous_shortest_path_flow) - demandModeB;

                            //sets this flow on corresponding edges on the graph
                            for (int j = 0; j < number_of_edges; j++) { // for all edges on graph
                                for (int k = 0; k < shortest_path.length; k++) {// for all edges on the maximal path
                                    shortest_path[k].setFlow(shortest_path_flow);
                                    if (graph.getEdges().get(j).getId() == shortest_path[k].getId()) {//finds the corresponding edge on the graph
                                        graph.getEdges().get(j).setFlow(shortest_path_flow);}
                                }
                            }
                            //sets the shortest path flow on corresponding edges on the used path set list
                            for (int j = 0; j < UsedPathSetList.size(); j++) {// for all paths in the path set
                                for (int k =0; k <UsedPathSetList.get(j).length; k++){// for all edges of the list j-th element
                                    for (int l = 0; l < shortest_path.length; l++) {// for all edges on the shortest path
                                        if (UsedPathSetList.get(j)[k].getId() == shortest_path[l].getId()) {//finds the corresponding edge
                                            UsedPathSetList.get(j)[k].setFlow(shortest_path_flow);
                                        }}}}


                            // if the flow on the maximal path is zero, the path is unused again and it must be dropped from the used path set list
                            for (int j = 0; j < UsedPathSetList.size(); j++) {
                                    if(UsedPathSetList.get(j)[0].getFlow() <= 0.01){
                                        PathsToRemove.add(UsedPathSetList.get(j));
                                    }
                            }
                            UsedPathSetList.removeAll(PathsToRemove);
                        }

                        if (shortest_path_travel_cost > excessDemandCost) {

                            //maximal_path_flow = previous_maximal_path_flow - (1/(max_path_edges_derivatives_sum+excess_demand_cost_derivatives_sum+bus_paths_edges_travel_costs_derivatives_sum))*
                              //      (max_path_travel_cost -excessDemandCost);

                            maximal_path_flow = previous_maximal_path_flow - (1/(max_path_edges_derivatives_sum+excess_demand_cost_derivatives_sum))*
                                    (max_path_travel_cost -excessDemandCost);

                            //maximal_path_flow = (Math.max(0.01,previous_maximal_path_flow - (1/(max_path_edges_derivatives_sum+excess_demand_cost_derivatives_sum+bus_paths_edges_travel_costs_derivatives_sum))*
                              //      (max_path_travel_cost -excessDemandCost)));
                            //sets this flow on corresponding edges on the graph
                            for (int j = 0; j < number_of_edges; j++) { // for all edges on graph
                                for (int k = 0; k < max_path.length; k++) {// for all edges on the maximal path
                                    max_path[k].setFlow(maximal_path_flow);
                                    if (graph.getEdges().get(j).getId() == max_path[k].getId()) {//finds the corresponding edge on the graph
                                        graph.getEdges().get(j).setFlow(maximal_path_flow);
                                    }
                                }
                            }

                            //sets this flow on corresponding edges on the used path set list
                            for (int j = 0; j < UsedPathSetList.size(); j++) {// for all paths in the path set
                                for (int k =0; k <UsedPathSetList.get(j).length; k++){// for all edges of the list j-th element
                                    for (int l = 0; l < max_path.length; l++) {// for all edges on the maximal path
                                        if (UsedPathSetList.get(j)[k].getId() == max_path[l].getId()) {//finds the corresponding edge
                                            UsedPathSetList.get(j)[k].setFlow(maximal_path_flow);
                                        }}}}

                            for (int j = 0; j < UsedPathSetList.size(); j++) {// for all paths from path set
                                other_paths_flow_sum += UsedPathSetList.get(j)[0].getFlow();
                            }
                            demandModeB = demandTotal -other_paths_flow_sum;

                            // if the flow on the maximal path is zero, the path is unused again and it must be dropped from the used path set list
                            for (int j = 0; j < UsedPathSetList.size(); j++) {
                                if(UsedPathSetList.get(j)[0].getFlow() <=0.01){
                                    PathsToRemove.add(UsedPathSetList.get(j));
                                }
                            }
                            UsedPathSetList.removeAll(PathsToRemove);
                        }
                        //update costs
                        for (int j = 0; j < number_of_edges; j++) {// for all edges of the graph
                            edgesCosts_copy[j] = costFunction.calculateCost(graph.getEdges().get(j).getFlow(),graph.getEdges().get(j).getCapacity(),graph.getEdges().get(j).getCost());// store the values into the array
                        }

            }
                }

            }

            // increment iteration counter
            currentIteration++;
            if ( currentIteration > iterationsCount ) {break; }
        }
    }
//Getters and setters
    /**
     * Performs callback indicating that the computation has been finished
     */
    protected void computationFinished() {
        callbackInvokable.computationFinished();
    }
    /**
     * Returns the maximal number of iterations
     * @return the maximal number of iterations
     */
    public int getIterationsCount() {
        return iterationsCount;
    }
    /**
     * Returns the maximal desired epsilon
     * @return the maximal desired epsilon
     */
    public double getEpsilon() {
        return epsilon;
    }
    /**
     * Sets the epsilon of this assignment program
     * @param  epsilon the new convergence criterion
     */
    public void setEpsilon(double  epsilon) {
        this.epsilon =  epsilon;
    }
    /**
     * Returns the current iteration
     * @return the current iteration
     */
    public int getCurrentIteration() {
        return currentIteration;
    }
    /**
     * Returns the maximal desired epsilon
     * @return the maximal desired epsilon
     */
    public double getTheta() {
        return theta;
    }
    /**
     * Sets the theta of this assignment program
     * @param  theta the new logit function parameter
     */
    public void setTheta(double  theta) {
        this.theta =  theta;
    }

    /**
     * Returns the walking speed of the passenger
     * @return the walkingSpeed
     */
    public double getWalkingSpeed() {
        return walkingSpeed;
    }
    /**
     * Sets the walkingSpeed of this assignment program
     * @param  walkingSpeed the new walking speed of passenger
     */
    public void setWalkingSpeed(double  walkingSpeed) {
        this.walkingSpeed =  walkingSpeed;
    }
    /**
     * Returns the average waiting time of the passenger at the stop
     * @return the average_waiting_time of the passenger at the stop
     */
    public double getAverage_waiting_time() {
        return average_waiting_time;
    }
    /**
     * Sets the average waiting time of the passenger at the stop
     * @param average_waiting_time of the passenger at the stop
     */
    public void setAverage_waiting_time(double  average_waiting_time) {
        this.average_waiting_time =  average_waiting_time;
    }
    /**
     * Returns the average dwell time of the vehicle at the stop
     * @return the average_dwell_time of the vehicle at the stop
     */
    public double getAverage_dwell_time() {
        return average_dwell_time;
    }
    /**
     * Sets the average dwell time of the vehicle at the stop
     * @param average_dwell_time of the vehicle at the stop
     */
    public void setAverage_dwell_time(double  average_dwell_time) {
        this.average_dwell_time =  average_dwell_time;
    }
    /**
     * Returns the average dwell time of the vehicle at the stop
     * @return the average_dwell_time of the vehicle at the stop
     */
    public double getAverage_boarding_time() {
        return average_boarding_time;
    }
    /**
     * Sets the average boarding time of the passengers when entering vehicle at the stop
     * @param average_boarding_time of the passengers when entering vehicle at the stop
     */
    public void setAverage_boarding_time(double  average_boarding_time) {
        this.average_boarding_time =  average_boarding_time;
    }

}
