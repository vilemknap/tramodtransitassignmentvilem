package com.kolovsky.java;

import java.io.Serializable;

/**
 * An edge of the graph representing the road traffic network
 * @author Tomas Potuzak
 */
public class TmEdge implements Comparable<TmEdge>, TmCopyable<TmEdge>, Serializable {
    /** The ID of the graph edge */
    protected int id;
    /** The index of the graph edge in the list of all edges */
    protected int index;
    /** The graph node where this graph edge starts */
    protected TmNode startNode;
    /** The graph node where this graph edge ends */
    protected TmNode endNode;
    /** The capacity of this graph edge */
    protected double capacity;
    /** The cost of this graph edge */
    protected double cost;
    /** Determines whether this graph edge is valid (i.e. is considered to exist and is used for computations) */
    protected boolean valid;
    /** The flow of the traffic road (graph edge) */
    protected double flow;
    /** The outer node, which is set only if this edge is an inner edge - a structure for implementing turn restrictions */
    protected TmNode outerNode;
    /** Dummy variable for public transport line occurrence*/
    protected int hasTransit;
    /** Dummy variable for public transport stop occurrence*/
    protected int hasStop;

    /** Dummy variable,1 if the source node of the edge is connected to trip generation zone */
    protected int is_source_connector;

    /** Dummy variable,1 if the source node of the edge is connected to trip attraction zone */
    protected int is_target_connector;

    /** The length of the link  for node transfer calculation */
    protected double length;

    public static final int DEFAULT_HAS_TRANSIT = 0;

    public static final int DEFAULT_HAS_STOP = 0;

    public static final int DEFAULT_IS_SOURCE_CONNECTOR= 0;

    public static final int DEFAULT_IS_TARGET_CONNECTOR= 0;



    public static final double DEFAULT_LENGTH = 1;


    /**
     * Creates a new graph edge with specified ID, capacity, cost, validity, transint dummy and length
     * @param id the ID of the graph edge
     * @param capacity the capacity of the graph edge
     * @param cost the cost of the graph edge
     * @param valid indicates whether the edge is valid
     * @param hasTransit indicates whether the edge has transit line
     * @param hasStop indicates whether the edge has transit line
     * @param is_source_connector indicates whether source node of the edge is connected to zone
     * @param is_target_connector indicates whether target node of the edge is connected to zone
     * @param length of the edge
     */
    public TmEdge(int id, double capacity, double cost, boolean valid, int hasTransit, int hasStop, int is_source_connector, int is_target_connector,double length) {
        this.id = id;
        this.capacity = capacity;
        this.cost = cost;
        this.valid = valid;
        this.hasTransit = hasTransit;
        this.hasStop = hasStop;
        this.is_source_connector = is_source_connector;
        this.is_target_connector = is_target_connector;
        this.length = length;

        //TODO temporal solution of edge validity - better would be to use TmEdge.isValid() in all algorithms - planned in future
        if (!valid) {
            this.cost += 10.0;
        }
    }

    /**
     * Returns <code>true</code> if this edge is an inner edge - a structure for implementation of turn restrictions. Returns <code>false</code> otherwise
     * @return <code>true</code> if this edge is an inner edge - a structure for implementation of turn restrictions. Returns <code>false</code> otherwise
     */
    public boolean isInnerEdge() {
        return (outerNode != null);
    }

    //Getters and setters

    /**
     * Returns the ID of this graph edge
     * @return the ID of this graph edge
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the ID of this graph edge
     * @param id the new ID of this graph edge
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Returns the index of this graph edge
     * @return the index of this graph edge
     */
    public int getIndex() {
        return index;
    }

    /**
     * Sets the index of this graph edge
     * @param index the new index of this graph edge
     */
    public void setIndex(int index) {
        this.index = index;
    }

    /**
     * Returns the start node of this graph edge
     * @return the start node of this graph edge
     */
    public TmNode getStartNode() {
        return startNode;
    }

    /**
     * Sets the start node of this graph edge
     * @param startNode the new start node of this graph edge
     */
    public void setStartNode(TmNode startNode) {
        this.startNode = startNode;
    }

    /**
     * Returns the end node of this graph edge
     * @return the end node of this graph edge
     */
    public TmNode getEndNode() {
        return endNode;
    }

    /**
     * Sets the end node of this graph edge
     * @param endNode the new end node of this graph edge
     */
    public void setEndNode(TmNode endNode) {
        this.endNode = endNode;
    }

    /**
     * Returns the capacity of this graph edge
     * @return the capacity of this graph edge
     */
    public double getCapacity() {
        return capacity;
    }

    /**
     * Sets the capacity of this graph edge
     * @param capacity the new capacity of this graph edge
     */
    public void setCapacity(double capacity) {
        this.capacity = capacity;
    }

    /**
     * Returns the cost of this graph edge
     * @return the cost of this graph edge
     */
    public double getCost() {
        return cost;
    }

    /**
     * Sets the cost of this graph edge
     * @param cost the new cost of this graph edge
     */
    public void setCost(double cost) {
        this.cost = cost;
    }

    /**
     * Returns <code>true</code> if this edge is valid. Returns <code>false</code> otherwise
     * @return <code>true</code> if this edge is valid. Returns <code>false</code> otherwise
     */
    public boolean isValid() {
        return valid;
    }

    /**
     * Sets whether this edge is valid
     * @param valid value indicating whether this edge shall be valid or not
     */
    public void setValid(boolean valid) {
        this.valid = valid;
    }

    /**
     * Returns the flow of this graph edge
     * @return the flow of this graph edge
     */
    public double getFlow() {
        return flow;
    }

    /**
     * Sets the flow of this graph edge
     * @param flow the new flow of this graph edge
     */
    public void setFlow(double flow) {
        this.flow = flow;
    }

    /**
     * Returns the outer node, which is set only when this edge is an inner edge. Otherwise, the value is <code>null</code>
     * @return the outer node, which is set only when this edge is an inner edge. Otherwise, the value is <code>null</code>
     */
    public TmNode getOuterNode() {
        return outerNode;
    }

    /**
     * Sets the outer node of this edge
     * @param outerNode the new outer node of this edge
     */
    public void setOuterNode(TmNode outerNode) {
        this.outerNode = outerNode;
    }
    /**
     * Returns the transit dummy of this graph edge
     * @return the transit dummy of this graph edge
     */
    public int getHasTransit() {
        return hasTransit;
    }

    /**
     * Sets the transit dummy of this graph edge
     * @param hasTransit the new flow of this graph edge
     */
    public void setHasTransit(int hasTransit) {
        this.hasTransit = hasTransit;
    }

    /**
     * Returns the stop dummy of this graph edge
     * @return the stop dummy of this graph edge
     */
    public int getHasStop() {
        return hasStop;
    }

    /**
     * Sets the stop dummy of this graph edge
     * @param hasStop the new flow of this graph edge
     */
    public void sethasStop(int hasStop) {
        this.hasStop = hasStop;
    }

    /**
     * Returns the source node zone dummy of this graph edge
     * @return the source node zone dummy of this graph edge
     */
    public int getIs_source_connector() {
        return is_source_connector;
    }

    /**
     * Sets the source node zone dummy of this graph edge
     * @param is_source_connector the source node zone dummy of this graph edge
     */
    public void setIs_source_connector(int is_source_connector) {
        this.is_source_connector = is_source_connector;
    }

    /**
     * Returns the target node zone dummy of this graph edge
     * @return the target node zone dummy of this graph edge
     */
    public int getIs_target_connector() {
        return is_target_connector;
    }

    /**
     * Sets the target node zone dummy of this graph edge
     * @param is_target_connector the target node zone dummy of this graph edge
     */
    public void setIs_target_connector(int is_target_connector) {
        this.is_target_connector = is_target_connector;
    }

    /**
     * Returns the length of this graph edge
     * @return the length of this graph edge
     */
    public double getLength() {
        return length;
    }

    /**
     * Sets the length of this graph edge
     * @param length the new flow of this graph edge
     */
    public void setLength(double length) {
        this.length = length;
    }

    //Inherited methods

    @Override
    public TmEdge getDeepCopy() {
        return null;
    }

    @Override
    public TmEdge getShallowCopy() {
        return null;
    }

    @Override
    public TmEdge getMixedCopy() {
        TmEdge copy = new TmEdge(id, capacity, cost, valid, hasTransit, hasStop,is_source_connector,is_target_connector,length);

        copy.cost = cost;
        copy.index = index;
        copy.flow = flow;

        return copy;
    }

    @Override
    public int compareTo(TmEdge other) {
        if (id < other.id) {
            return TmComparisonUtils.THIS_LOWER;
        }
        else if (id > other.id) {
            return TmComparisonUtils.THIS_HIGHER;
        }
        else {
            return TmComparisonUtils.EQUAL;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof TmEdge) {
            TmEdge other = (TmEdge) o;

            if (id == other.id) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "Edge ID: " + id + ", index: " + index;
    }
}

