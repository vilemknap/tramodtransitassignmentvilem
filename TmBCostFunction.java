package com.kolovsky.java;

/**
 * Cost function for calculation of edge cost including integral and derivation
 * @author Tomas Potuzak
 */
public class TmBCostFunction {
    /** Default coefficient of the cost function  */
    public static final double DEFAULT_COEFFICIENT = 0.15;
    /** Default exponent of the cost function */
    public static final int DEFAULT_EXPONENT = 4;
    /** The coefficient of this cost function */
    protected double coefficient;
    /** The coefficient of this cost function */
    protected int exponent;

    /**
     * Create a new cost function with specified coefficient and exponent
     * @param coefficient the coefficient of this cost function
     * @param exponent the exponent of this cost function
     */
    public TmBCostFunction(double coefficient, int exponent) {
        this.coefficient = coefficient;
        this.exponent = exponent;
    }

    /**
     * Creates a new cost function with default values of coefficient and exponent
     */
    public TmBCostFunction() {
        this(DEFAULT_COEFFICIENT, DEFAULT_EXPONENT);
    }

    /**
     * Calculates and returns cost value of an edge based on its specified flow, capacity, and initial cost values
     * @param flow the flow of an edge
     * @param capacity the capacity of an edge
     * @param initialCost the initial cost of an edge
     * @return cost value of an edge
     * @throws ArithmeticException if the result if infinite (i.e., positive or negative infinity or not a number)
     */
    public double calculateCost(double flow, double capacity, double initialCost) throws ArithmeticException {
        double cost = initialCost * (1 + coefficient * TmMathUtils.power(flow / capacity, exponent));
        //double cost = initialCost * (1 + coefficient * Math.pow(flow / capacity, exponent));

        if (Double.isNaN(cost) || Double.isInfinite(cost)) {
            throw new ArithmeticException("Cost cannot be infinite.");
        }

        return cost;
    }

    /**
     * Calculates and returns cost value derivation of an edge based on its specified flow, capacity, and initial cost values
     * @param flow the flow of an edge
     * @param capacity the capacity of an edge
     * @param initialCost the initial cost of an edge
     * @return cost value derivation of an edge
     * @throws ArithmeticException if the result if infinite (i.e., positive or negative infinity or not a number)
     */
    public double calculateDerivation(double flow, double capacity, double initialCost) throws ArithmeticException {
        double derivation = coefficient * exponent * initialCost * TmMathUtils.power(flow, exponent - 1) / TmMathUtils.power(capacity, exponent);
        //double derivation = coefficient * exponent * initialCost * Math.pow(flow, exponent - 1) / Math.pow(capacity, exponent);

        if (Double.isNaN(derivation) || Double.isInfinite(derivation)) {
            throw new ArithmeticException("Derivation cannot be infinite.");
        }

        return derivation;
    }

    /**
     * Calculates and returns cost value integral of an edge based on its specified flow, capacity, and initial cost values
     * @param flow the flow of an edge
     * @param capacity the capacity of an edge
     * @param initialCost the initial cost of an edge
     * @return cost value derivation of an edge
     * @throws ArithmeticException if the result if infinite (i.e., positive or negative infinity or not a number)
     */
    public double calculateIntegral(double flow, double capacity, double initialCost) throws ArithmeticException {
        double integral = initialCost * ((coefficient * flow * TmMathUtils.power(flow / capacity, exponent)) / (exponent + 1) + flow);
        //double integral = initialCost * ((coefficient * flow * Math.pow(flow / capacity, exponent)) / (exponent + 1) + flow);

        if (Double.isNaN(integral) || Double.isInfinite(integral)) {
            throw new ArithmeticException("Integral cannot be infinite.");
        }

        return integral;
    }

    //Getters and setters

    /**
     * Returns the coefficient of this cost function
     * @return the coefficient of this cost function
     */
    public double getCoefficient() {
        return coefficient;
    }

    /**
     * Sets the coefficient of this cost function
     * @param coefficient the new coefficient, which shall be set to this cost function
     */
    public void setCoefficient(double coefficient) {
        this.coefficient = coefficient;
    }

    /**
     * @return the exponent
     */
    public int getExponent() {
        return exponent;
    }

    /**
     * @param exponent the exponent to set
     */
    public void setExponent(int exponent) {
        this.exponent = exponent;
    }

    //Inherited methods

    @Override
    public String toString() {
        return "cost = cost0 * (1 + " + coefficient + " * (flow / capacity) ^ " + exponent;
    }
}
