package com.kolovsky.modeler

import com.kolovsky.java._
import org.junit.{Ignore, Test}

class ModelerTestExample {
  @Ignore
  @Test
  def example(): Unit ={
    val db = new TmDbConnector("")
    val graph = db.readGraph("tm_pilsen")
    val odm = db.readOriginDestinationMatrix(graph)

    val pair = odm.getOriginDestinationPair(6,8 )
    println(pair.getFlow)
    pair.setFlow(5.0)


    val algorithm = new TmParallelBStaticTrafficAssignment(odm,3, 1E-3, 2, new MyCallback())
    algorithm.setSynchronousComputation(true)
    algorithm.calculateEdgesFlowsAssignment()
    val flows = algorithm.getAssignedEdgesFlows()
    println(flows.toList)
  }

  @Test
  def example2(): Unit = {
    //NETWORK CREATION:
    val graph = new TmGraph("aaa")
    val source = new TmNode(0)
    val target = new TmNode(1)
    val middle1 = new TmNode(2)
    val middle2 = new TmNode(3)
    val middle3 = new TmNode(4)
    val middle4 = new TmNode(5)
    graph.addNode(source)
    graph.addNode(middle1)
    graph.addNode(middle2)
    graph.addNode(middle3)
    graph.addNode(middle4)
    graph.addNode(target)

    // Direct OD edges
    val first = new TmEdge(0, 1200, 9, true, 0, 0, 1,1,50)
    val second = new TmEdge(1, 1000, 7, true, 0, 0, 1,1,40)
    graph.addEdge(first)
    graph.addEdge(second)
    source.addOutgoingEdge(first)
    source.addOutgoingEdge(second)
    target.addIncomingEdge(first)
    target.addIncomingEdge(second)
    //Transit line
    val third = new TmEdge(2,950,3,true,0,0,1,0,2)
    val fourth = new TmEdge(3,1000,1.5,true,1,1,0,0,2.5)
    val fifth = new TmEdge(4,1000,2,true,1,0,0,0,3)
    val sixth = new TmEdge(5,900,1,true,1,1,0,0,2)
    val seventh = new TmEdge(5,950,1,true,1,1,0,1,2)
    //Connecting coresponding edges to nodes
    source.addOutgoingEdge(third)
    middle1.addIncomingEdge(third)
    middle1.addOutgoingEdge(fourth)
    middle2.addIncomingEdge(fourth)
    middle2.addOutgoingEdge(fifth)
    middle3.addIncomingEdge(fifth)
    middle3.addOutgoingEdge(sixth)
    middle4.addIncomingEdge(sixth)
    middle4.addOutgoingEdge(seventh)
    target.addIncomingEdge(seventh)
    //Connecting coresponding nodes to edges
    first.setStartNode(source)
    first.setEndNode(target)
    second.setStartNode(source)
    second.setEndNode(target)
    third.setStartNode(source)
    third.setEndNode(middle1)
    fourth.setStartNode(middle1)
    fourth.setEndNode(middle2)
    fifth.setStartNode(middle2)
    fifth.setEndNode(middle3)
    sixth.setStartNode(middle3)
    sixth.setEndNode(middle4)
    seventh.setStartNode(middle4)
    seventh.setEndNode(target)
    //Adding nodes to graph
    graph.initializeNodesAccessTable()
    //Adding edges to graph
    graph.initializeEdgesAccessTable()

    //LINKING OD MATRIX WITH GRAPH
    val odm_car = new TmOriginDestinationMatrix(graph)
    val demand_total = new TmOriginDestinationPair(new TmZone(0, graph.getNodeById(0)), new TmZone(1, graph.getNodeById(1)), 7100)
    odm_car.addOriginDestinationPair(demand_total)
    odm_car.initializeDestinationColumnAccessTable()
    odm_car.initializeOriginRowAccessTable()

    //INSTATNTIATING ALGORITHM CLASS
    val algorithm = new TmExcessDemandGradientProjectionAssignment(odm_car,7,1E-3,2,new MyCallback(),0.5,10,3/60,1/360,1/60)
    //PERFORMING CALCULATION
    algorithm.CalculateGPA()
  }

  @Ignore
  @Test
  def example3(): Unit = {
    import scala.io.Source
    val source = Source.fromFile("diplomka/Gis/Edges2.csv")

    for (line <- source.getLines) {
      val cols = line.split(",").map(_.trim)
      // do whatever you want with the columns here
      println(s"${cols(0)}|${cols(1)}|${cols(2)}|${cols(3)}")

      val a = new TmEdge(1,2,3,true,1,1,1,0,1)
    }
  }


}
