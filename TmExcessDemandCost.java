package com.kolovsky.java;
import java.lang.Math;

public class TmExcessDemandCost {
    /** Default transit cost value in the excess demandcost function  */
    public static final double DEFAULT_TRAVEL_COST_OF_MODE_B = 5;
    /** Default logit paramater of the excess demand cost function */
    public static final double DEFAULT_THETA = 1;
    /** Default total demand of the excess demand cost function */
    public static final double DEFAULT_DEMAND_TOTAL = 0.001;

    /** Default transit demand of the excess demand cost function */
    public static final double DEFAULT_DEMAND_OF_MODE_B = 0.005;
    /** The logit parameter of this cost function */
    protected double theta;
    /** The coefficient of this cost function */
    protected int exponent;
    /** Total demand */
    protected double demand_total;
    /** Excess demand (Demand of mode B) */
    protected double demand_of_mode_B;
    /** Travel cost of mode B */
    protected double travel_cost_of_mode_B;
    /**
     * Create a new Excess demand cost function with specified logit parameter and constant travel cost
     * @param theta logit parameter
     * @param travel_cost_of_mode_B constant travel cost of mode B
     */
    public TmExcessDemandCost(double theta, double travel_cost_of_mode_B,double demand_total, double demand_of_mode_B) {
        this.theta = theta;
        this.travel_cost_of_mode_B = travel_cost_of_mode_B;
        this.demand_total = demand_total;
        this.demand_of_mode_B = demand_of_mode_B;
    }

    /**
     * Creates a new excess demand cost function with default values of coefficient and exponent
     */
    public TmExcessDemandCost() {
        this(DEFAULT_TRAVEL_COST_OF_MODE_B, DEFAULT_THETA,DEFAULT_DEMAND_TOTAL,DEFAULT_DEMAND_OF_MODE_B);
    }

    /**
     * Calculates and returns excess demand cost based on its specified total demand, demand of mode B,logit parameter and constant travel cost of mode B
     * @param theta logit parameter for the modal split function
     * @param travel_cost_of_mode_B the constant travel cost of public transport for now
     * @param demand_total flow represents total demand from origin to destination (qrs)
     * @param demand_of_mode_B demand of mode B
     * @return excess demand cost of an od pair
     * @throws ArithmeticException if the result if infinite (i.e., positive or negative infinity or not a number)
     * @throws ArithmeticException if theta or demand_of_mode_B are 0 (i.e., positive or negative infinity or not a number)
     */
    public double calculate_Excess_demand_Cost(double theta, double travel_cost_of_mode_B, double demand_total, double demand_of_mode_B) throws ArithmeticException {
        double excess_demand_cost = ((1 / theta) * Math.log(demand_of_mode_B / (demand_total - demand_of_mode_B))) + travel_cost_of_mode_B;

        if (Double.isNaN(excess_demand_cost) || Double.isInfinite(excess_demand_cost)) {
            throw new ArithmeticException("Cost cannot be infinite.");
        }
        if(theta == 0) {
            throw new ArithmeticException("theta cannot be 0 (division by 0)");
        }
        if(demand_of_mode_B == 0) {
            throw new ArithmeticException("demand_of_mode_B cannot be 0 (undefined for log)");
        }
        return excess_demand_cost;
    }

    /**
     * Calculates and returns excess demand cost derivative based on its specified total demand, demand of mode B,logit parameter and constant travel cost of mode B
     * @param theta logit parameter for the modal split function
     * @param travel_cost_of_mode_B the constant travel cost of public transport for now
     * @param demand_total flow represents total demand from origin to destination (qrs)
     * @param demand_of_mode_B demand of mode B
     * @return excess demand cost derivative of an od pair
     * @throws ArithmeticException if the result is infinite (i.e., positive or negative infinity or not a number)
     * @throws ArithmeticException if theta or demand_of_mode_B are 0 (i.e., positive or negative infinity or not a number)
     */
    public double calculateExcess_demand_cost_Derivative(double theta, double travel_cost_of_mode_B, double demand_total, double demand_of_mode_B) throws ArithmeticException {
        double excess_demand_cost_derivative = demand_total/(-theta*TmMathUtils.power(demand_of_mode_B, 2) + theta * demand_of_mode_B * demand_total) ;


        if (Double.isNaN(excess_demand_cost_derivative) || Double.isInfinite(excess_demand_cost_derivative)) {
            throw new ArithmeticException("Derivation cannot be infinite.");
        }
        if(theta == 0) {
            throw new ArithmeticException("theta cannot be 0");
        }

        return excess_demand_cost_derivative;
    }
    /**
     * Calculates and returns excess demand cost integral based on its specified total demand, demand of mode B, logit parameter and constant travel cost of mode B
     * @param theta logit parameter for the modal split function
     * @param travel_cost_of_mode_B the constant travel cost of public transport for now
     * @param demand_total flow represents total demand from origin to destination (qrs)
     * @param demand_of_mode_B demand of mode B
     * @return excess demand cost integral of an od pair
     * @throws ArithmeticException if the result is infinite (i.e., positive or negative infinity or not a number)
     * @throws ArithmeticException if theta or demand_of_mode_B are 0 (i.e., positive or negative infinity or not a number)
     */
    public double calculateExcess_demand_cost_Integral(double theta, double travel_cost_of_mode_B, double demand_total, double demand_of_mode_B) throws ArithmeticException {
        double excess_demand_cost_integral = (1/theta)*demand_of_mode_B*(theta*travel_cost_of_mode_B* Math.log(demand_of_mode_B/(demand_total- demand_of_mode_B))) + demand_total * Math.log(demand_of_mode_B-demand_total);
        //double derivation = coefficient * exponent * initialCost * Math.pow(flow, exponent - 1) / Math.pow(capacity, exponent);

        if (Double.isNaN(excess_demand_cost_integral) || Double.isInfinite(excess_demand_cost_integral)) {
            throw new ArithmeticException("Integral cannot be infinite.");
        }
        if(theta == 0) {
            throw new ArithmeticException("theta cannot be 0");
        }

        return excess_demand_cost_integral;
    }
    //Getters and setters
    /**
     * Returns the logit parameter of this excess demand cost function
     * @return the theta of this excess demand cost function
     */
    public double getTheta() {
        return theta;
    }

    /**
     * Sets the logit parameter of this excess demand cost function
     * @param theta the new logit parameter, which shall be set to this excess demand cost function
     */
    public void setTheta(double theta) {
        this.theta = theta;
    }

    /**
     * Returns the travel_cost_of_mode_B of this excess demand cost function
     * @return the travel_cost_of_mode_B of this excess demand cost function
     */
    public double getTravel_cost_of_mode_B() {
        return  travel_cost_of_mode_B;
    }

    /**
     * Sets the travel_cost_of_mode_B of this excess demand cost function
     * @param  travel_cost_of_mode_B the new travel_cost_of_mode_B parameter, which shall be set to this excess demand cost function
     */
    public void setTravel_cost_of_mode_B(double  travel_cost_of_mode_B) {
        this.travel_cost_of_mode_B =  travel_cost_of_mode_B;
    }

    /**
     * Returns the demand_total of this excess demand cost function
     * @return the demand_total of this excess demand cost function
     */
    public double getDemand_total() {
        return demand_total;
    }

    /**
     * Sets the demand_total of this excess demand cost function
     * @param demand_total the new demand_total parameter, which shall be set to this excess demand cost function
     */
    public void setDemand_total(double demand_total) {
        this.demand_total =  demand_total;
    }

    /**
     * Returns the demand_of_mode_B of this excess demand cost function
     * @return the demand_of_mode_B of this excess demand cost function
     */
    public double getDemand_of_mode_B() {
        return  demand_of_mode_B;
    }

    /**
     * Sets the demand_of_mode_B of this excess demand cost function
     * @param  demand_of_mode_B the new demand_total parameter, which shall be set to this excess demand cost function
     */
    public void setDemand_of_mode_B(double  demand_of_mode_B) {
        this.demand_of_mode_B =  demand_of_mode_B;
    }


}


